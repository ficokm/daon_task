package com.filip.daontask.database.repository;

import com.filip.daontask.database.model.Gate;
import com.filip.daontask.enums.GateStatus;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalTime;
import java.util.List;

public interface GateRepository extends JpaRepository<Gate, Integer> {

    @Query("SELECT gate FROM Gate gate " +
            "WHERE gate.status = :status " +
            "   AND availableFrom <= :now" +
            "   AND availableTo > :now")
    List<Gate> findByStatus(@Param("status") GateStatus status, Sort sort,
                            @Param("now") LocalTime now);
}
