package com.filip.daontask.database.model;

import com.filip.daontask.enums.GateStatus;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.time.LocalTime;

@Entity
@Data
@Table(name = "gates")
public class Gate {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "available_from")
    private LocalTime availableFrom;

    @Column(name = "available_to")
    private LocalTime availableTo;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private GateStatus status;

    @Version
    @Column(name = "version")
    private Long version;

    @Column(name = "position")
    private Integer position;
}
