package com.filip.daontask.service.mapper;

import com.filip.daontask.controller.dto.GateDTO;
import com.filip.daontask.database.model.Gate;
import org.springframework.stereotype.Component;

@Component
public class GateMapper {

    public GateDTO map(Gate gate) {
        GateDTO gateDTO = new GateDTO();
        if (gate == null) {
            return gateDTO;
        }

        gateDTO.setId(gate.getId());
        gateDTO.setName(gate.getName());

        return gateDTO;
    }
}
