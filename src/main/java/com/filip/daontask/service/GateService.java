package com.filip.daontask.service;

import com.filip.daontask.controller.dto.GateDTO;
import com.filip.daontask.controller.errors.BadRequestException;
import com.filip.daontask.controller.errors.NotAcceptableException;
import com.filip.daontask.database.model.Gate;
import com.filip.daontask.database.repository.GateRepository;
import com.filip.daontask.enums.GateStatus;
import com.filip.daontask.service.mapper.GateMapper;
import com.filip.daontask.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class GateService {

    private final GateRepository gateRepository;

    private final GateMapper gateMapper;

    public void updateWorkingHours(Integer gateId, String from, String to) {
        LocalTime start = DateUtils.toLocalTime(from);
        LocalTime end = DateUtils.toLocalTime(to);

        if (!DateUtils.checkTime(start, end)) {
            throw new NotAcceptableException("End time has to be after start time", "error.time.bad");
        }

        Gate gate = gateRepository.findById(gateId).orElseThrow(() ->
                new BadRequestException("Given gate id doesn't exist", "error.gate.404"));
        gate.setAvailableFrom(start);
        gate.setAvailableTo(end);

        gateRepository.save(gate);
    }

    public GateDTO checkGates(String flightNumber) {
        log.debug("Checking gates for flight number: " + flightNumber);
        List<Gate> gates = gateRepository.findByStatus(GateStatus.AVAILABLE, Sort.by("position"), this.now());

        for (Gate gate : gates) {
            try {
                gate.setStatus(GateStatus.IN_USE);
                gateRepository.save(gate);
                return gateMapper.map(gate);
            } catch (ObjectOptimisticLockingFailureException e) {
                log.debug("Conflict happened!!!");
            }
        }
        throw new NotAcceptableException("No gates available", "error.gate.in-user");
    }

    public void changeStatus(GateStatus status, Integer gateId) {
        Gate gate = gateRepository.findById(gateId).orElseThrow(() -> new BadRequestException("Given gateId doesn't exist in database"));

        if (gate.getStatus() == status) {
            throw new NotAcceptableException("Gate already in status:" + status.toString(), "error.stats.already");
        }
        gate.setStatus(status);

        gateRepository.save(gate);
    }

    private LocalTime now() {
        return Instant.now().atZone(ZoneOffset.UTC).toLocalTime();
    }
}
