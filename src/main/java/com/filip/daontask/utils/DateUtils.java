package com.filip.daontask.utils;

import com.filip.daontask.controller.errors.NotAcceptableException;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

public class DateUtils {

    public static boolean checkTime(LocalTime start, LocalTime end) {
        return end.isAfter(start);
    }

    public static LocalTime toLocalTime(String s) {
        try {
            return LocalTime.parse(s);
        } catch (DateTimeParseException e) {
            throw new NotAcceptableException("Wrong time format", "error.time.wrong-format");
        }
    }
}
