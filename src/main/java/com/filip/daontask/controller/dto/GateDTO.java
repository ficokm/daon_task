package com.filip.daontask.controller.dto;

import lombok.Data;

@Data
public class GateDTO {

    private Integer id;

    private String name;
}
