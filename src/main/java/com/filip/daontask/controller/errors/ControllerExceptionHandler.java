package com.filip.daontask.controller.errors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ErrorResponse handleNotAcceptable(NotAcceptableException e) {
        log.warn("Request media type not acceptable message: {}", e.getMessage());
        return new ErrorResponse(e.getDeveloperMessage(), e.getUserMessage(), e.getMap());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleBadRequest(BadRequestException e) {
        log.warn("Request media type not acceptable message: {}", e.getMessage());
        return new ErrorResponse(e.getDeveloperMessage(), e.getUserMessage(), e.getMap());
    }
}
