package com.filip.daontask.controller.errors;

import java.util.Map;

public class BadRequestException extends BaseException {

    public BadRequestException(String developerMessage) {
        super(developerMessage);
    }

    public BadRequestException(String developerMessage, String userMessage) {
        super(developerMessage, userMessage);
    }

    public BadRequestException(String developerMessage, String userMessage, Map<String, Object> map) {
        super(developerMessage, userMessage, map);
    }
}
