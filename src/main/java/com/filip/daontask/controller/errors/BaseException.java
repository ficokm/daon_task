package com.filip.daontask.controller.errors;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class BaseException extends RuntimeException {

    private String developerMessage;

    private String userMessage;

    private Map<String, Object> map = new HashMap<>();

    public BaseException(String developerMessage) {
        super(developerMessage);
        this.developerMessage = developerMessage;
    }

    public BaseException(String developerMessage, String userMessage) {
        super(developerMessage);
        this.developerMessage = developerMessage;
        this.userMessage = userMessage;
    }

    public BaseException(String developerMessage, String userMessage, Map<String, Object> map) {
        super(developerMessage);
        this.developerMessage = developerMessage;
        this.userMessage = userMessage;
        this.map = map;
    }
}