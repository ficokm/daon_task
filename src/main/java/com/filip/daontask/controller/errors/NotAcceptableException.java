package com.filip.daontask.controller.errors;

import java.util.Map;

public class NotAcceptableException extends BaseException {

    public NotAcceptableException(String developerMessage) {
        super(developerMessage);
    }

    public NotAcceptableException(String developerMessage, String userMessage) {
        super(developerMessage, userMessage);
    }

    public NotAcceptableException(String developerMessage, String userMessage, Map<String, Object> map) {
        super(developerMessage, userMessage, map);
    }
}
