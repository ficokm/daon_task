package com.filip.daontask.controller.errors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class ErrorResponse {

    private String developerMessage;

    private String details;

    private Map<String, Object> map;

    public ErrorResponse(String developerMessage, String userMessage, Map<String, Object> map) {
        this.developerMessage = developerMessage;
        this.details = userMessage;
        this.map = map;
    }
}