package com.filip.daontask.controller;

import com.filip.daontask.controller.dto.GateDTO;
import com.filip.daontask.controller.errors.BadRequestException;
import com.filip.daontask.controller.errors.NotAcceptableException;
import com.filip.daontask.enums.GateStatus;
import com.filip.daontask.service.GateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/gate")
public class GateController {

    private final GateService gateService;


    /**
     * Uses for updating working hours for given gate
     *
     * @param gateId given gate id
     * @param from   start from
     * @param to     end to
     * @return success
     * @throws NotAcceptableException strings (time || to) are not in required format 'hh:mm:ss'
     *                                || start time is greater then end time
     * @throws BadRequestException    given gate doesn't exist
     */
    @PutMapping
    public ResponseEntity<Void> updateWorkingHours(@RequestParam Integer gateId,
                                                   @RequestParam String from,
                                                   @RequestParam String to) throws NotAcceptableException, BadRequestException {
        gateService.updateWorkingHours(gateId, from, to);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Uses for getting available gate for parking for given flight number
     *
     * @param flightNumber given flight number
     * @return available gate
     * @throws NotAcceptableException when there is not gate available
     */
    @GetMapping
    public ResponseEntity<GateDTO> checkGate(@RequestParam String flightNumber) throws NotAcceptableException {
        return new ResponseEntity<>(gateService.checkGates(flightNumber), HttpStatus.OK);
    }


    /**
     * Uses for setting gate in available status
     *
     * @param gateId given gate id
     * @return success
     * @throws NotAcceptableException when given gate is already in available status
     * @throws BadRequestException    given gate doesn't exist
     */
    @PutMapping("/available")
    public ResponseEntity<Void> setAvailable(@RequestParam Integer gateId) throws NotAcceptableException, BadRequestException {
        gateService.changeStatus(GateStatus.AVAILABLE, gateId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
