package com.filip.daontask.enums;

public enum GateStatus {
    AVAILABLE,
    IN_USE
}
