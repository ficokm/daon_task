package com.filip.daontask.service;

import com.filip.daontask.controller.dto.GateDTO;
import com.filip.daontask.database.model.Gate;
import com.filip.daontask.database.repository.GateRepository;
import com.filip.daontask.enums.GateStatus;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@Slf4j
@SpringBootTest(webEnvironment = NONE)
@RunWith(SpringRunner.class)
public class AssignFlightTest {

    @Autowired
    private GateService service;

    @Autowired
    private GateRepository gateRepository;

    @Test
    public void success_flight_assign() {
        this.creteGates(1);

        GateDTO gateDTO = service.checkGates("Regular flight");

        assertEquals(gateDTO.getName(), "Radiant gate 1");

        this.deleteGates();
    }

    @Test
    public void success_two_flight_assign() throws InterruptedException {
        this.creteGates(2);

        Thread thread = new Thread(() -> service.checkGates("Async flight"));
        thread.start();

        service.checkGates("Regular flight");
        thread.join();

        this.deleteGates();
    }


    private void creteGates(int number) {
        for (int i = 1; i <= number; i++) {
            Gate gate = new Gate();
            gate.setStatus(GateStatus.AVAILABLE);
            gate.setAvailableFrom(LocalTime.of(7, 0));
            gate.setAvailableTo(LocalTime.of(23, 0));
            gate.setPosition(i);
            gate.setId(i);
            gate.setName("Radiant gate " + i);
            gateRepository.save(gate);
        }
    }

    private void deleteGates() {
        gateRepository.deleteAll();
    }
}
