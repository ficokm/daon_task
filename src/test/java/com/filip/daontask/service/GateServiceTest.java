package com.filip.daontask.service;

import com.filip.daontask.controller.errors.BadRequestException;
import com.filip.daontask.controller.errors.NotAcceptableException;
import com.filip.daontask.database.model.Gate;
import com.filip.daontask.database.repository.GateRepository;
import com.filip.daontask.enums.GateStatus;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;

import static org.junit.Assert.assertThrows;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@Slf4j
@SpringBootTest(webEnvironment = NONE)
@RunWith(SpringRunner.class)
public class GateServiceTest {

    @Autowired
    private GateService service;

    @Autowired
    private GateRepository gateRepository;

    @Before
    public void setUp() {
        Gate gate = new Gate();
        gate.setStatus(GateStatus.AVAILABLE);
        gate.setAvailableFrom(LocalTime.of(9, 0));
        gate.setAvailableTo(LocalTime.of(20, 0));
        gate.setPosition(1);
        gate.setId(1);
        gate.setName("Radiant gate");
        gateRepository.save(gate);
    }

    @Test
    public void bad_time_format() {
        assertThrows(NotAcceptableException.class, () -> service.updateWorkingHours(1, "nice:try", "also:nice:try"));
    }

    @Test
    public void start_time_greater_then_end_time() {
        assertThrows(NotAcceptableException.class, () -> service.updateWorkingHours(1, "18:00", "15:00"));
    }

    @Test
    public void bad_gate_request() {
        assertThrows(BadRequestException.class, () -> service.updateWorkingHours(55, "10:00", "15:00"));
    }

    @Test
    public void success_update() {
        service.updateWorkingHours(1, "10:00", "20:00");
    }
}