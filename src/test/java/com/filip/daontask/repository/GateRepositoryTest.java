package com.filip.daontask.repository;


import com.filip.daontask.database.model.Gate;
import com.filip.daontask.database.repository.GateRepository;
import com.filip.daontask.enums.GateStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@SpringBootTest(webEnvironment = NONE)
@RunWith(SpringRunner.class)
public class GateRepositoryTest {

    @Autowired
    private GateRepository gateRepository;

    @Before
    public void setUp() throws Exception {
        Gate gate = new Gate();

        gate.setAvailableFrom(LocalTime.of(9, 0));
        gate.setAvailableTo(LocalTime.of(20, 0));
        gate.setName(this.getName(1));
        gate.setStatus(GateStatus.AVAILABLE);
        gate.setPosition(1);
        gateRepository.save(gate);

        Gate gate2 = new Gate();

        gate2.setAvailableFrom(LocalTime.of(9, 0));
        gate2.setAvailableTo(LocalTime.of(20, 0));
        gate2.setName(this.getName(2));
        gate2.setStatus(GateStatus.AVAILABLE);
        gate2.setPosition(2);
        gateRepository.save(gate2);

        Gate gate3 = new Gate();

        gate3.setAvailableFrom(LocalTime.of(9, 0));
        gate3.setAvailableTo(LocalTime.of(20, 0));
        gate3.setName(this.getName(3));
        gate3.setStatus(GateStatus.AVAILABLE);
        gate3.setPosition(3);
        gateRepository.save(gate3);
    }

    @Test
    public void concurrency_requests() {
        System.out.println("First request started");

        // given
        LocalTime now = Instant.now().atZone(ZoneOffset.UTC).toLocalTime();
        List<Gate> gates = gateRepository.findByStatus(GateStatus.AVAILABLE, this.getSort(), now);

        // when
        for (Gate gate : gates) {
            try {
                System.out.println("First request checking gate: " + gate.getName());
                // Call new method that will set one of the gates in status IN_USE
                if (gates.indexOf(gate) == 0) {
                    this.setInUser();
                }

                gate.setStatus(GateStatus.IN_USE);
                gateRepository.save(gate);
                System.out.println("First request can park to gate: " + gate.getName());
                return;
            } catch (Exception e) {
                System.out.println("Conflict happened!!!");
            }
        }

        // then
        gates = gateRepository.findByStatus(GateStatus.AVAILABLE, this.getSort(), now);
        assertEquals(gates.size(), 1);

        // and
        gates = gateRepository.findByStatus(GateStatus.IN_USE, this.getSort(), now);
        assertEquals(gates.size(), 2);
    }

    private void setInUser() {
        System.out.println("Second request started job");
        LocalTime now = Instant.now().atZone(ZoneOffset.UTC).toLocalTime();

        List<Gate> gates = gateRepository.findByStatus(GateStatus.AVAILABLE, this.getSort(), now);

        for (Gate gate : gates) {
            try {
                System.out.println("Second request checking gate: " + gate.getName());
                gate.setStatus(GateStatus.IN_USE);
                gateRepository.save(gate);
                System.out.println("Second request can park to gate: " + gate.getName());
                break;
            } catch (Exception e) {
                System.out.println("Conflict happened!!!");
            }
        }
    }

    private String getName(int number) {
        return "Gate " + number;
    }

    private Sort getSort() {
        return Sort.by("position");
    }
}
