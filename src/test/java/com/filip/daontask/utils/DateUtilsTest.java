package com.filip.daontask.utils;

import com.filip.daontask.controller.errors.NotAcceptableException;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class DateUtilsTest {

    @Test
    public void check_time_true() {
        assertTrue(DateUtils.checkTime(LocalTime.now(), LocalTime.now().plusHours(2)));
    }

    @Test
    public void check_time_false() {
        assertFalse(DateUtils.checkTime(LocalTime.now(), LocalTime.now().minusHours(2)));
    }

    @Test
    public void check_format_true() {
        LocalTime localTime = DateUtils.toLocalTime("15:30");

        LocalTime object = LocalTime.of(15, 30);

        assertEquals(localTime, object);
    }

    @Test
    public void check_format_false() {
        assertThrows(NotAcceptableException.class,() -> DateUtils.toLocalTime("no:time:given"));
    }
}
